# Python 3.7 #

Neste exemplo, utilizamos um container com versão linux conhecida por Alpine. 
Esta versão contém um tamanho reduzido e possui apenas os requisitos mínimos para execução de scripts python.
Note que a pasta /src contém um exemplo de código em python (hello world), que é executado no final.

### Comandos ###
docker-compose up

http://localhost:5000