FROM python:alpine3.7 
COPY ./src /src
WORKDIR /src
RUN pip install -r requirements.txt
EXPOSE 5000
CMD python ./index.py